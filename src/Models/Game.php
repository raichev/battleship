<?php
namespace Battleship\Models;

use Battleship\Models\Board;
use Battleship\Models\Ships\Ship;
use Exception;


class Game 
{
	public $config = [];
	public $board;
	public $ships = [];
	public $shots = 0;

	public function  __construct(array $config){
		$this->config = $config;
		$this->board = new Board();
		$this->board->initBoard($config['board']);
		foreach ($config['ships'] as $ship) {
			$className = "Battleship\\Models\\Ships\\".$ship;
			$ship = new $className;
			$this->board->addShip($ship);
			$this->ships[] = $ship;
		}

		$_SESSION['game'] = serialize($this);
	}

	/**
	 * @return int
	 */
	public function getShots()
	{
		return $this->shots;
	}

	/**
	 * @return int $userShots
	 */
	public function increaseShots()
	{
		$this->shots += 1;
	}

	/**
	 * @return Board
	 */
	public function getBoard()
	{
		return $this->board;
	}

	/**
	 * @return Ship
	 */
	public function getShip()
	{
		return $this->ships;
	}


	public function validate($input)
	{
		if(empty($input) || strlen($input) < 1 ){
		  	return  "*** Enter coordinates ***";
		} else if(!preg_match("(^[a-zA-Z][1-9]|10)", $input)){
			return  "*** Error ***";
		}

		return null;
	}

	public function getRequest()
	{
		if ($_POST && count($_POST)) {
			$request = [];
			foreach ($_POST as $var => $varValue) {
				$request[$var] = $varValue;
			}
			return $request;
		}

		return null;
	}

	public function setSymbol(string $searchingCoordinate, string $symbol)
	{
		foreach ($this->board->grid as $coordinates) {
	   		foreach ($coordinates as $coordinate) {
	   			if($coordinate['coordinates'] === $searchingCoordinate){

	   				$x = $coordinate['x'];
	   				$y = $coordinate['y'];
	   				$this->board->grid[$x][$y]['is_shoot'] = 1;
					$this->board->grid[$x][$y]['symbol'] = $symbol;
	   			}
	   		}
	   }

	   return $this->board->grid;
	}


	public function getDeadShipsCounter(){
		$deadShipCounter = 0;
		foreach ($this->getShip()  as $ship) {
			if($ship->isSunk()) {
				$deadShipCounter++;
			}
		}

		return $deadShipCounter;
	}


}