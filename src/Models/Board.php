<?php
namespace Battleship\Models;

use Battleship\Models\Ships\Ship;
use Exception;


class Board 
{
	
	public $grid = [];

	public function initBoard(array $config)
	{
		$alphabet = 65; // A
		for ($x=1; $x <= $config['height'] ; $x++) {
			for($y = 1; $y <= $config['width']; $y++) {
				$this->grid[$x][$y]['x'] = $x;
				$this->grid[$x][$y]['y'] = $y;
				$this->grid[$x][$y]['ship'] = 0;
				$this->grid[$x][$y]['symbol'] = '.';
				$this->grid[$x][$y]['coordinates'] = chr($alphabet).$y;
				$this->grid[$x][$y]['is_shoot'] = 0;
			}

			$alphabet++;
		}
	}

	public function initShowCommandBoard()
	{
		foreach($this->grid as $coordinates){
			foreach ($coordinates as $coordinate) {
				if($this->grid[$coordinate['x']][$coordinate['y']]['ship'] == 1) {
					$this->grid[$coordinate['x']][$coordinate['y']]['symbol'] = 'X';
				} else {
					$this->grid[$coordinate['x']][$coordinate['y']]['symbol'] = '';
				}
			}
		}
		return $this->grid;
	}

	static public function renderBoard(array $grid, array $config) 
	{
		echo "<table>";
		echo "<thead>";
				echo "<th></th>";
			for($i = 1; $i <= $config['width']; $i++){
				echo "<th>".$i."</th>";
			}
		echo "</thead>";
		$alphabet = 65; //A;
		for($i = 1; $i <= count($grid); $i++) {
			echo "<tr>";
			echo "<td>".chr($alphabet)."</td>";
			for($j = 1; $j <= count($grid[$i]); $j++) {
				echo "<td>".$grid[$i][$j]['symbol']."</td>";
			}
			echo "</tr>";
			$alphabet++;
		}
		echo "</table>";
	}

	public function addShip(Ship $ship)
	{
		$genRandBoardCoordinates = $this->getRandomPosition();
		$orientiration = $this->getRandomOrientation();
		$coordinates = $this->checkSpaceForShip($ship, $genRandBoardCoordinates, $orientiration);

	}

	public function checkSpaceForShip(Ship $ship, array $genRandBoardCoordinates, int $orientiration)
	{
		$shipSize = $ship->getSize();
		$shipCoordinates = [];
		if($orientiration == 0) { // horizontal
			if($shipSize <=  count($this->grid[$genRandBoardCoordinates['x']])  - $genRandBoardCoordinates['y'] ){
				$tempArray = [];
				for ($i = $genRandBoardCoordinates['y']; $i < ($shipSize + $genRandBoardCoordinates['y']); $i++){
					if($this->grid[$genRandBoardCoordinates['x']][$i]['ship'] == '0'){
						array_push($tempArray, $this->grid[$genRandBoardCoordinates['x']][$i]);
					} 
				}
				if(count($tempArray) == $shipSize ){
					foreach ($tempArray as $value) {
						$this->grid[$value['x']][$value['y']]['ship'] = 1;
					}
	 				$ship->setShipCoordinates(array_column($tempArray, 'coordinates'));			
				} else {
					$this->addShip($ship);
				}
			}  else {
				$this->addShip($ship);
			}
	
		} else  { // vartical
			$tempArray = [];
			if($shipSize <=  count($this->grid[$genRandBoardCoordinates['y']])  - $genRandBoardCoordinates['x'] ){
				for ($i = $genRandBoardCoordinates['x']; $i < ($shipSize + $genRandBoardCoordinates['x']); $i++) {
					if($this->grid[$i][$genRandBoardCoordinates['y']]['ship'] == 0){
						array_push($tempArray, $this->grid[$i][$genRandBoardCoordinates['y']]);
					} 
				}
				if(count($tempArray) == $shipSize ) {
					foreach ($tempArray as $value) {
						$this->grid[$value['x']][$value['y']]['ship'] = 1;
					}
	 				$ship->setShipCoordinates(array_column($tempArray, 'coordinates'));			
				} else {
					$this->addShip($ship);
				}
			} else {
				$this->addShip($ship);
			}
		}


	}

	public function getRandomOrientation()
	{
		return rand(0,1); // 0 - horizontal, 1 - vertical
	}

	public function getRandomPosition()
	{
		return ['x' => rand(1,10), 'y' => rand(1,10)];
	}

	public function showCoordinateInput(){
		echo "<form action='index.php' method='post'><span>Enter coordinates (row, col), e.g. A5 <input type='text' name='coordinates' required> 
		<input type='submit' value='Submit'>
		</form>";
	}


}

