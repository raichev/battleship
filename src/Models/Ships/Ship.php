<?php
namespace Battleship\Models\Ships;

abstract class Ship 
{
	protected $size;
	protected $life;
	protected $shipCoordinates;

	/**
	 * @return int $size
	 */
	public function getSize() 
	{
		return $this->size;
	}

	/**
	 * @return array $shipCoordinates
	 */
	public function setShipCoordinates(array $shipCoordinates)
	{
		return $this->shipCoordinates = $shipCoordinates;
	}

	/**
	 * @return array $shipCoordinates
	 */
	public function getShipCoordinates()
	{
		return $this->shipCoordinates;
	}


	/**
	 * @return int $life
	 */
	public function isSunk()
	{
		return $this->life === 0;
	}

	/**
	 * @return int $life
	 */
	public function shipHit()
	{
		return $this->life -= 1;
	}

	/**
	 * @return string $coordinate
	 */
	public function shipCoordinateExist(string $coordinate)
	{
		return in_array($coordinate, $this->shipCoordinates);
	}




}

