<?php
namespace Battleship\Models\Ships;

class Destroyer extends Ship
{
	protected $size = 4;
	protected $life = 4;
}