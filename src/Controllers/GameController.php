<?php
namespace Battleship\Controllers;

use Battleship\Models\Ships\Ship;
use Battleship\Models\Board;
use Battleship\Models\Game;
use Exception;

class GameController{
	private $config;
	private $board;

	public function __construct(
		array $config
	){
		$this->config = $config;
		$this->play($this->config);
	}

	public function play()
	{
		$session = isset($_SESSION['game']) ? unserialize($_SESSION['game']) : null;
		if($session) {
			$game = $session;
		} else {
			$game = new Game($this->config);
			$_SESSION['game'] = serialize($game);
		}
		$this->board = new Board;

		$this->shoot();
	}

	public function shoot()
	{
		$game = unserialize($_SESSION['game']);
		$request = $game->getRequest();

		if(isset($request['coordinates'])){
			$input = $request['coordinates'];
			if ($input == 'show') {
				$_SESSION['game'] = serialize($game);
				$game->board->initShowCommandBoard();
			} else if($input == 'reset'){
				session_destroy();
				$game = new Game($this->config);
			} else {
				$game->increaseShots();
				$input = strtoupper($input);
				if($game->validate($input) == null){
					foreach ($game->getShip() as $ship) {
						if($ship->shipCoordinateExist($input)){
							$hitShip = $ship;
							break;
						} else {
							$hitShip = null;
						}
					}
					if(!is_null($hitShip)){
						$hitShip->shipHit();
						$game->board->grid = $game->setSymbol($input, 'X');
						if($hitShip->isSunk()){
							echo "*** SUNK ***";
						} else {
							echo "*** HIT ***";
						}
					} else {
						echo "*** MISS ***";
						$game->board->grid = $game->setSymbol($input, '-');
					}
					$_SESSION['game'] = serialize($game);
				} else {
					echo $game->validate($input);
				}
			}
		}
		$deadShipCounter = $game->getDeadShipsCounter();
		
		if($deadShipCounter == count($this->config['ships'])){
			echo '<h1>Well done!</h1>';
			echo 'You\'ve completed the game in '.$game->shots.' shots.';
			echo '<p><a href="index.php">Start New Game?</a></p>';
			session_destroy();
			exit;
		} else {
			$this->board->renderBoard($game->getBoard()->grid, $this->config['board']);
			$this->board->showCoordinateInput();
		}

	}

	
}