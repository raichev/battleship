<?php

require  __DIR__ .'/vendor/autoload.php';
$config = require  __DIR__ .'/config/config.php';
session_start();

use Battleship\Models\Game;
use Battleship\Controllers\GameController;

$game = new GameController($config);



